from os import environ
from datetime import datetime
from time import sleep
import discord
from discord.ext import tasks, commands
import asyncio


class MyClient(discord.Client):
    def __init__(self):
        super().__init__(intents=discord.Intents.default())
        self.synced = False

    async def on_ready(self):
        await self.wait_until_ready()
        if not self.synced:
            await tree.sync(guild=discord.Object(id=self.guilds[0].id))
            self.synced = True

        print(environ.get("bot_name") + " is up !")
        print(f"Logged in as {self.user}.")
        print('------')

        asyncio.ensure_future(self.start_loop())
        channel_id = self.get_guild(self.guilds[0].id).system_channel.id
        await self.get_channel(channel_id).send("Hello, I'm awake !")

    async def start_loop(self):
        while int(datetime.now().strftime("%H")) != (int(datetime.now().strftime("%M")) + 1):
            print("still waiting")
            await asyncio.sleep(30)
        print('Loop is started !')
        self.mirror_hours.start()

    @tasks.loop(seconds=3660)
    async def mirror_hours(self):
        await self.get_channel(channel_id).send("C'est bientôt l'heure de toucher du rouge et son nez !")


client = MyClient()
tree = discord.app_commands.CommandTree(client)


@tree.command(name="setup", description="Sending channel configuration", guild=discord.Object(id=int(environ.get('guild_id'))))
async def self(interaction: discord.Interaction):
    global channel_id
    channel_id = interaction.channel_id
    await interaction.response.send_message(f"{client.get_channel(channel_id).name} is the new send channel !")

client.run(environ.get("bot_token"))
